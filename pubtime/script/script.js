/*jslint browser:true */

window.requestAnimFrame = (function () {
return window.requestAnimationFrame    ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function (callback) {
        window.setTimeout(callback, 1000 / 60);
    };
})();

var canvas = document.querySelector('body > canvas');
var settingUp = 20;

var pubCooldown = 2;
var pubTime = 0;

var pubStrings;
if (location.hash != '#no') {
    pubStrings = [
        'Pub?',
        '?',
        'pub',
        'PUB!',
        'pub?',
        '!'
    ];
} else {
    pubStrings = [
        'nah',
        'nope',
        'no',
        ':(',
        'D:',
        ':c'
    ];
}

function resizeCanvas() {
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
}
resizeCanvas();
window.addEventListener('resize', resizeCanvas);

function render() {
    var ctx = canvas.getContext('2d'),
        pubSize,
        pubX,
        pubY,
        pubHue;

    ctx.fillStyle = 'RGBA(0, 0, 0, 0.1)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    pubTime--;
    if (pubTime <= 0) {
        pubTime = pubCooldown;

        pubSize = 50 + Math.random() * 72;
        pubX = Math.random() * canvas.width;
        pubY = Math.random() * canvas.height - pubSize;
        pubHue = ((pubX + pubY) / (canvas.width + canvas.height)) * 360;

        ctx.font = pubSize + 'px sans-serif';
        ctx.fillStyle = 'hsla(' + pubHue + ', 80%, 80%, 1)';
        ctx.textAlign = pubX > canvas.width - canvas.width / 4 ? 'right' : (pubX < canvas.width / 4 ? 'left' : 'center');
        ctx.fillText(pubStrings[Math.floor(Math.random() * pubStrings.length)], pubX, pubY + pubSize);
    }

    if (settingUp > 0) {
        settingUp--;
        ctx.fillStyle = '#050505';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }
}

(function animate(){
    window.requestAnimFrame(animate);
    render();
})();
